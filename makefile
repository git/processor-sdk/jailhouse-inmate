ifeq ($(RULES_MAKE), )
include $(PDK_INSTALL_PATH)/ti/build/Rules.make
else
include $(RULES_MAKE)
endif

ifeq ($(OS),Windows_NT)
export ROOT_DIR=$(CURDIR)
else
export ROOT_DIR=$(shell pwd)
endif
export INSTALL_DIR ?= $(ROOT_DIR)

.PHONY: all led_test pruss_test icss_emac_test

all: led_test pruss_test icss_emac_test

clean: led_test_clean pruss_test_clean icss_emac_test_clean

install: led_test_install pruss_test_install icss_emac_test_install

led_test:
	$(MAKE) -C ./baremetal/led

led_test_clean:
	$(MAKE) -C ./baremetal/led clean

led_test_install:
	$(MAKE) -C ./baremetal/led install

pruss_test:
	$(MAKE) -C ./rtos/pru-icss

pruss_test_clean:
	$(MAKE) -C ./rtos/pru-icss clean

pruss_test_install: 
	$(MAKE) -C ./rtos/pru-icss install

icss_emac_test:
	$(MAKE) -C ./rtos/icss_emac

icss_emac_test_clean:
	$(MAKE) -C ./rtos/icss_emac clean

icss_emac_test_install: 
	$(MAKE) -C ./rtos/icss_emac install
