/*
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 */

#include <ti/csl/csl_types.h>
#include <ti/csl/cslr_device.h>

#include <ti/drv/uart/UART.h>
#include <ti/drv/uart/UART_stdio.h>
#include <ti/drv/uart/soc/UART_soc.h>
#include <ti/board/board.h>

struct jh_ipc_t {
	unsigned int inmate2root;
	unsigned int root2inmate;
};

struct jh_ipc_t *ipc_ptr = (struct jh_ipc_t *)0xeef00000;

/**************** I2C stubs *********************************************/
void Board_setDigOutput(unsigned char ledData)
{
	static unsigned char old_val = 0xff;

	if (ledData != old_val) {
		old_val = ledData;
		UART_printf("Board_setDigOutput - 0x%02x\n", old_val);
	}

	ipc_ptr->inmate2root = ledData;
}

Board_STATUS Board_getIDInfo(Board_IDInfo *info)
{
	*info = (Board_IDInfo) {
		.header         = "Jailhouse",
			.boardName      = "AM572x Inmate",
			.version        = "v0.7",
			.serialNum      = "0001"
	};

	return BOARD_SOK;
}

Board_STATUS Board_moduleClockInit()
{
    CSL_l4per_cm_core_componentRegs *l4PerCmReg =
        (CSL_l4per_cm_core_componentRegs *) CSL_MPU_L4PER_CM_CORE_REGS;

    CSL_FINST(l4PerCmReg->CM_L4PER2_PRUSS1_CLKCTRL_REG,
        L4PER_CM_CORE_COMPONENT_CM_L4PER2_PRUSS1_CLKCTRL_REG_MODULEMODE, DISABLED);

    CSL_FINST(l4PerCmReg->CM_L4PER2_PRUSS1_CLKCTRL_REG,
        L4PER_CM_CORE_COMPONENT_CM_L4PER2_PRUSS1_CLKCTRL_REG_MODULEMODE, ENABLE);

    while(CSL_L4PER_CM_CORE_COMPONENT_CM_L4PER2_L4_PER2_CLKCTRL_REG_IDLEST_FUNC !=
        CSL_FEXT(l4PerCmReg->CM_L4PER2_PRUSS1_CLKCTRL_REG,
        L4PER_CM_CORE_COMPONENT_CM_L4PER2_PRUSS1_CLKCTRL_REG_IDLEST));

    CSL_FINST(l4PerCmReg->CM_L4PER2_PRUSS2_CLKCTRL_REG,
        L4PER_CM_CORE_COMPONENT_CM_L4PER2_PRUSS2_CLKCTRL_REG_MODULEMODE, DISABLED);

    CSL_FINST(l4PerCmReg->CM_L4PER2_PRUSS2_CLKCTRL_REG,
        L4PER_CM_CORE_COMPONENT_CM_L4PER2_PRUSS2_CLKCTRL_REG_MODULEMODE, ENABLE);

    while(CSL_L4PER_CM_CORE_COMPONENT_CM_L4PER2_L4_PER2_CLKCTRL_REG_IDLEST_FUNC !=
        CSL_FEXT(l4PerCmReg->CM_L4PER2_PRUSS2_CLKCTRL_REG,
        L4PER_CM_CORE_COMPONENT_CM_L4PER2_PRUSS2_CLKCTRL_REG_IDLEST));

    return BOARD_SOK;
}

Board_STATUS Board_init(Board_initCfg cfg)
{
	#define BOARD_UART_INSTANCE 2
	Board_STATUS ret = BOARD_SOK;
	UART_HwAttrs uart_hwAttrs;

	/* Make sure non-interrupt mode is used for UART */
	/* Get the UART default configuration */
	UART_socGetInitCfg(BOARD_UART_INSTANCE, &uart_hwAttrs);
	/* Disabling interrupt mode, forcing UART to use the polling mode */
	uart_hwAttrs.enableInterrupt = 0;
	/* Write back the config */
	UART_socSetInitCfg(BOARD_UART_INSTANCE, &uart_hwAttrs);

	if (cfg & BOARD_INIT_MODULE_CLOCK)
		ret = Board_moduleClockInit();
	if (ret != BOARD_SOK)
		return ret;

	if (cfg & BOARD_INIT_UART_STDIO)
		UART_stdioInit(BOARD_UART_INSTANCE);

	return ret;
}

/*************** GPIO stubs **********************/
void  Board_setTriColorLED(uint32_t gpioLeds, uint8_t value)
{
	// UART_printf("GPIO_OUT %d - %d\n", gpioLeds, value);
}

void Board_readHVS(uint8_t *switches)
{
	*switches = ipc_ptr->root2inmate;
}
