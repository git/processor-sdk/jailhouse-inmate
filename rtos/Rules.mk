# cscope.rules

FULL_CSRCS = $(foreach f,$(CSRCS),$(firstword $(wildcard $(VPATH:%=%/$f)) not-found:$f))

.PHONY: cscope build_deps
cscope: build_deps
	@rm -f cscope.*
	@for F in $(FULL_CSRCS); do echo $$F >> cscope.files; done
	@cat .headers.1.dep >> cscope.files
	@sort -u cscope.files > .headers.dep
	@mv .headers.dep cscope.files
	@rm -f .headers.dep .headers.1.dep
	@cscope -b -k
	@echo "cscope.out has been built"

DEPS = $(patsubst %.c,%.d,$(CSRCS))

build_deps:
	@rm -f *.dep
	@for F in $(DEPS); do \
		$(MAKE) $$F; \
		done
	@grep -v "\.o:" .headers.dep | sort -u | cut -f2 -d' ' > .headers.1.dep

%.d: %.c
	$(CC) $(CFLAGS) -MM $< -MF $(notdir $@)
	@cat $(notdir $@) >> .headers.dep
	@rm -f $(notdir $@)
