/******************************************************************************
 * Copyright (c) 2017 Texas Instruments Incorporated - http://www.ti.com
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

#include <ti/csl/csl_types.h>
#include <ti/csl/cslr_device.h>

#include <ti/drv/uart/UART.h>
#include <ti/drv/uart/UART_stdio.h>
#include <ti/board/board.h>

Board_STATUS Board_moduleClockInit();

Board_STATUS Board_init(Board_initCfg cfg)
{
	Board_STATUS ret = BOARD_SOK;

	if (cfg & BOARD_INIT_MODULE_CLOCK)
		ret = Board_moduleClockInit();
	if (ret != BOARD_SOK)
		return ret;

	if (cfg & BOARD_INIT_UART_STDIO)
		UART_stdioInit(2);

	if (cfg & BOARD_INIT_ICSS_ETH_PHY)
		ret = Board_icssEthConfig();
	if (ret != BOARD_SOK)
		return ret;

	return ret;
}

Board_STATUS Board_moduleClockInit()
{
    CSL_l4per_cm_core_componentRegs *l4PerCmReg =
        (CSL_l4per_cm_core_componentRegs *) CSL_MPU_L4PER_CM_CORE_REGS;

    CSL_FINST(l4PerCmReg->CM_L4PER2_PRUSS1_CLKCTRL_REG,
        L4PER_CM_CORE_COMPONENT_CM_L4PER2_PRUSS1_CLKCTRL_REG_MODULEMODE, DISABLED);

    CSL_FINST(l4PerCmReg->CM_L4PER2_PRUSS1_CLKCTRL_REG,
        L4PER_CM_CORE_COMPONENT_CM_L4PER2_PRUSS1_CLKCTRL_REG_MODULEMODE, ENABLE);

    while(CSL_L4PER_CM_CORE_COMPONENT_CM_L4PER2_L4_PER2_CLKCTRL_REG_IDLEST_FUNC !=
        CSL_FEXT(l4PerCmReg->CM_L4PER2_PRUSS1_CLKCTRL_REG,
        L4PER_CM_CORE_COMPONENT_CM_L4PER2_PRUSS1_CLKCTRL_REG_IDLEST));

    CSL_FINST(l4PerCmReg->CM_L4PER2_PRUSS2_CLKCTRL_REG,
        L4PER_CM_CORE_COMPONENT_CM_L4PER2_PRUSS2_CLKCTRL_REG_MODULEMODE, DISABLED);

    CSL_FINST(l4PerCmReg->CM_L4PER2_PRUSS2_CLKCTRL_REG,
        L4PER_CM_CORE_COMPONENT_CM_L4PER2_PRUSS2_CLKCTRL_REG_MODULEMODE, ENABLE);

    while(CSL_L4PER_CM_CORE_COMPONENT_CM_L4PER2_L4_PER2_CLKCTRL_REG_IDLEST_FUNC !=
        CSL_FEXT(l4PerCmReg->CM_L4PER2_PRUSS2_CLKCTRL_REG,
        L4PER_CM_CORE_COMPONENT_CM_L4PER2_PRUSS2_CLKCTRL_REG_IDLEST));

    return BOARD_SOK;
}
