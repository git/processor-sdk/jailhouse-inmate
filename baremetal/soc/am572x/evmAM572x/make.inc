
COMMON_ASRCS = entry.S
COMMON_CSRCS = gic.c

COMMON_OBJS = $(patsubst %.S,%.o,$(COMMON_ASRCS))
COMMON_OBJS += $(patsubst %.c,%.o,$(COMMON_CSRCS))

