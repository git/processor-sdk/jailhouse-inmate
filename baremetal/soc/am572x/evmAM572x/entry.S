@******************************************************************************
@ Copyright (C) 2017 Texas Instruments Incorporated - http://www.ti.com/
@
@
@  Redistribution and use in source and binary forms, with or without
@  modification, are permitted provided that the following conditions
@  are met:
@
@    Redistributions of source code must retain the above copyright
@    notice, this list of conditions and the following disclaimer.
@
@    Redistributions in binary form must reproduce the above copyright
@    notice, this list of conditions and the following disclaimer in the
@    documentation and/or other materials provided with the
@    distribution.
@
@    Neither the name of Texas Instruments Incorporated nor the names of
@    its contributors may be used to endorse or promote products derived
@    from this software without specific prior written permission.
@
@  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
@  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
@  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
@  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
@  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
@  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
@  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
@  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
@  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
@  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
@  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
@
@******************************************************************************

@****************************** Global Symbols*********************************

	.global _entry
	.global _reset	
        .global _stack
        .global _bss_start
        .global _bss_end
        .global start_boot
	.global INTCCommonIntrHandler

@************************ Internal Definitions ********************************
@
@ Define the stack sizes for different modes. The user/system mode will use
@ the rest of the total stack size
@
        .set  UND_STACK_SIZE, 0x8
        .set  ABT_STACK_SIZE, 0x8
        .set  FIQ_STACK_SIZE, 0x8
        .set  IRQ_STACK_SIZE, 0x1000
        .set  SVC_STACK_SIZE, 0x8

@
@ to set the mode bits in CPSR for different modes
@
        .set  MODE_USR, 0x10
        .set  MODE_FIQ, 0x11
        .set  MODE_IRQ, 0x12
        .set  MODE_SVC, 0x13
        .set  MODE_ABT, 0x17
        .set  MODE_UND, 0x1B
        .set  MODE_SYS, 0x1F

        .equ  I_F_BIT, 0xC0
        .equ  MASK_SVC_NUM, 0x1

        .text
        .code 32

@******************************************************************************
_reset:	b	_entry
	b	undef_handler		
	b	svc_handler
	b	pabt_handler
	b	dabt_handler
	b	unused_handler
	b	irq_handler
	b	fiq_handler

.weak undef_handler
undef_handler:
	b	.

.weak svc_handler
svc_handler:
	b	.

.weak pabt_handler
pabt_handler:
	b	.

.weak dabt_handler
dabt_handler:
	b	.

.weak unused_handler
unused_handler:
	b	.

irq_handler:
        sub       r14, r14, #4
        stmfd     r13!, {r0-r3, r12, r14}
        mrs       r12, spsr
        vmrs      r1, fpscr
        vmrs      r2, fpexc
        stmfd     r13!, {r1-r2, r12}
        vstmdb    r13!, {d0-d7}
        vstmdb    r13!, {d16-d31}

        bl        INTCCommonIntrHandler

        vldmia    r13!, {d16-d31}
        vldmia    r13!, {d0-d7}
        ldmfd     r13!, {r1-r2, r12}
        msr       spsr, r12
        vmsr      fpscr, r1
        vmsr      fpexc, r2

        ldmfd     r13!, {r0-r3, r12, pc}^

.weak fiq_handler
fiq_handler:
	b	.

@==========================================================================
_entry:	
	ldr	r0, =_reset
	mcr	p15, 0, r0, c12, c0, 0		@ VBAR

@ Set up the Stack for Undefined mode
        ldr   r0, =_stack
        msr   cpsr_c, #MODE_UND|I_F_BIT
        mov   sp, r0
        sub   r0, r0, #UND_STACK_SIZE

@ Set up the Stack for abort mode
        msr   cpsr_c, #MODE_ABT|I_F_BIT
        mov   sp, r0
        sub   r0, r0, #ABT_STACK_SIZE

@ Set up the Stack for FIQ mode
@
        msr   cpsr_c, #MODE_FIQ|I_F_BIT
        mov   sp, r0
        sub   r0, r0, #FIQ_STACK_SIZE

@ Set up the Stack for IRQ mode
@
        msr   cpsr_c, #MODE_IRQ|I_F_BIT
        mov   sp, r0
        sub   r0, r0, #IRQ_STACK_SIZE

@ Set up the Stack for SVC mode
@
        msr   cpsr_c, #MODE_SVC|I_F_BIT
        mov   sp, r0
        sub   r0, r0, #SVC_STACK_SIZE

@ Set up the Stack for USer/System mode
@
        msr   cpsr_c, #MODE_SYS|I_F_BIT    @ change to system mode
        mov   sp, r0                       @ write the stack pointer

@ Invalidate and Enable Branch Prediction
        mov     r0, #0
        mcr     p15, #0, r0, c7, c5, #6
        isb
        mrc     p15, #0, r0, c1, c0, #0
        orr     r0, r0, #0x00000800
        mcr     p15, #0, r0, c1, c0, #0

@
@ Enable Neon/VFP Co-Processor
@
        mrc	p15, #0, r1, c1, c0, #2		@ r1 = Access Control Register
        orr	r1, r1, #(0xf << 20)		@ enable full access for p10,11
        mcr	p15, #0, r1, c1, c0, #2		@ Access Control Register = r1
        mov	r1, #0
        mcr	p15, #0, r1, c7, c5, #4		@ flush prefetch buffer
        mov	r0,#0x40000000
        fmxr	fpexc, r0                     @ Set Neon/VFP Enable bit

@
@ Clear the BSS section here
@
        ldr   r0, =_bss_start           @ Start address of BSS
        ldr   r1, =(_bss_end - 0x04)    @ End address of BSS
        mov   r2, #0
loop:
        str   r2, [r0], #4              @ Clear one word in BSS
        cmp   r0, r1
        ble   loop                      @ Clear till BSS end

	bl	main			@ Get the address of main
	b	.	

        .end
