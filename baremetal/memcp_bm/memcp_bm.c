/*
 *  Copyright (C) 2017 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include <ti/csl/tistdtypes.h>
#include <ti/csl/csl_a15.h>
#include <ti/csl/csl_armGicAux.h>

#include <stdio.h>
#include <ti/csl/soc.h>

#define USE_MMU

#ifdef USE_MMU
CSL_ArmGicDistIntrf distrIntrf;
CSL_ArmGicCpuIntrf gCpuIntrf;

CSL_A15MmuLongDescObj mmuObj __attribute__((aligned(16 * 1024)));

CSL_A15MmuLongDescAttr mmuAttr0;
CSL_A15MmuLongDescAttr mmuAttr1;

static void mmu_init(void)
{
	uint32_t phyAddr = 0U;
	int32_t cacheType;

	cacheType = CSL_a15GetCacheType();

	if(cacheType & CSL_A15_CACHE_TYPE_ALL)
	{
		CSL_a15InvAllInstrCache();
		CSL_a15DisableCache();
	}

	mmuObj.numFirstLvlEntires = CSL_A15_MMU_LONG_DESC_LVL1_ENTIRES;
	mmuObj.numSecondLvlEntires = CSL_A15_MMU_LONG_DESC_LVL2_ENTIRES;
	mmuObj.mairEntires = CSL_A15_MMU_MAIR_LEN_BYTES;
	mmuObj.mairAttr[0] = 0x44U;
	mmuObj.mairAttr[1] = 0x00U;
	mmuObj.mairAttr[2] = 0xFFU;
	CSL_a15SetMmuMair(0, mmuObj.mairAttr[0]);
	CSL_a15SetMmuMair(1, mmuObj.mairAttr[1]);
	CSL_a15SetMmuMair(2, mmuObj.mairAttr[2]);

	CSL_a15InitMmuLongDesc(&mmuObj);

	CSL_a15InitMmuLongDescAttrs(&mmuAttr0);
	CSL_a15InitMmuLongDescAttrs(&mmuAttr1);

	mmuAttr0.type = CSL_A15_MMU_LONG_DESC_TYPE_BLOCK;
	mmuAttr0.accPerm = 0U;
	mmuAttr0.shareable = 2U;
	mmuAttr0.attrIndx = 2U;

	for (phyAddr = 0x00000000U; phyAddr < 0x08000000U; phyAddr += 0x00200000U)
		CSL_a15SetMmuSecondLevelLongDesc(&mmuObj, (void *)phyAddr,
						 (void *)phyAddr, &mmuAttr0);

	mmuAttr1.type = CSL_A15_MMU_LONG_DESC_TYPE_BLOCK;
	mmuAttr1.accPerm = 0U;
	mmuAttr1.shareable = 0U;
	mmuAttr1.attrIndx = 1U;

	for (phyAddr = 0x10000000U; phyAddr < 0x60000000U; phyAddr += 0x200000U)
		CSL_a15SetMmuSecondLevelLongDesc(&mmuObj, (void *)phyAddr,
						 (void *)phyAddr, &mmuAttr1);

	CSL_a15EnableMmu();
	CSL_a15EnableCache();
}
#endif

#ifdef USE_GIC
static void gic_init(void)
{
	gCpuIntrf.gicDist = &distrIntrf;
	gCpuIntrf.cpuIntfBasePtr = (void *)0x48212000U;
	distrIntrf.distBasePtr = (void *)0x48211000U;
	gCpuIntrf.initStatus = (Uint32)FALSE;
	gCpuIntrf.gicDist->initStatus = (Uint32)FALSE;
	gCpuIntrf.pDefaultIntrHandlers = NULL; /* &CSL_armGicDefaultHandler; */
	gCpuIntrf.pDefaultUserParameter = NULL;

	CSL_armGicInit(&gCpuIntrf);
}
#endif

/*******************************************************************************/
#define evmAM572x
#define SOC_AM572X

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ti/drv/uart/UART.h>
#include <ti/drv/uart/soc/UART_soc.h>
#include <ti/drv/uart/UART_stdio.h>
#include <ti/csl/soc.h>

#include <ti/board/board.h>
#include <ti/board/src/evmAM572x/include/board_cfg.h>

#define NUMBER_OF_CYCLES        (3)

void memcopy_test(void);
	
int main(void)
{
	Board_initCfg boardCfg;
	UART_HwAttrs uart_hwAttrs;
	Osal_HwAttrs osal_hwAttrs;
#ifdef USE_MMU
	mmu_init();
#endif	
#ifdef USE_GIC	
	gic_init();
#endif

	/* Make sure non-interrupt mode is used for UART */
	/* Get the UART default configuration */
	UART_socGetInitCfg(BOARD_UART_INSTANCE, &uart_hwAttrs);
	/* Disabling interrupt mode, forcing UART to use the polling mode */
	uart_hwAttrs.enableInterrupt=0; 
	/* Write back the config */
	UART_socSetInitCfg(BOARD_UART_INSTANCE, &uart_hwAttrs);
	
	Osal_getHwAttrs(&osal_hwAttrs);
	osal_hwAttrs.hwAccessType=OSAL_HWACCESS_RESTRICTED;
	Osal_setHwAttrs(OSAL_HWATTR_SET_HWACCESS_TYPE,&osal_hwAttrs);
	
	boardCfg = BOARD_INIT_MODULE_CLOCK | BOARD_INIT_UART_STDIO;

    /* Set the OSAL hwAccess to restricted */

	Board_init(boardCfg);

	memcopy_test();
	return 0;
}

#define MB 1024 * 1024 * 4

char aaa[MB] __attribute__ ((aligned(4096)));
char bbb[MB] __attribute__ ((aligned(4096)));

void mcpy(void *dst, void *src, int csize);
void mcpy(void *dst, void *src, int csize)
{
	asm (
		"	push	{r3-r10}\n"
		"llll:	ldmia	r1!, {r3 - r10}\n"
		"	stmia	r0!, {r3 - r10}\n"
		"	subs	r2, r2, #32\n"
		"bge	llll\n"
		"	pop	{r3-r10}\n"
	);
}

void memcopy_test(void)
{
	int j, k;

	UART_printf("\nTest started 1\n");
	for(j = 0; j < 1000; j++) {
		for (k = 0; k < 16; k++)
			mcpy(bbb, aaa, sizeof(aaa));
	}
	UART_printf("\nTest end 2\n");
}
