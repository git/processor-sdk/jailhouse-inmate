/*
 *  Copyright (C) 2017 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include <ti/csl/tistdtypes.h>
#include <ti/csl/csl_a15.h>
#include <ti/csl/csl_armGicAux.h>

#include <stdio.h>
#include <ti/drv/gpio/GPIO.h>
#include <ti/csl/soc.h>
#include <ti/drv/gpio/soc/GPIO_v1.h>

#define USE_MMU

/* Port and pin number mask for LEDs.
   Bits 7-0: Pin number  and Bits 15-8: Port number */
#define LED1	(0x0708)
#define LED2	(0x0709)
#define LED3	(0x070E)
#define LED4	(0x070F)

/* GPIO Driver board specific pin configuration structure */
GPIO_PinConfig gpioPinConfigs[] = {
	LED1 | GPIO_CFG_OUTPUT,
	LED2 | GPIO_CFG_OUTPUT,
	LED3 | GPIO_CFG_OUTPUT,
	LED4 | GPIO_CFG_OUTPUT
};

/* GPIO Driver call back functions */
GPIO_CallbackFxn gpioCallbackFunctions[] = {
    NULL,
    NULL,
    NULL,
    NULL
};

/* GPIO Driver configuration structure */
GPIO_v1_Config GPIO_v1_config = {
    gpioPinConfigs,
    gpioCallbackFunctions,
    sizeof(gpioPinConfigs) / sizeof(GPIO_PinConfig),
    sizeof(gpioCallbackFunctions) / sizeof(GPIO_CallbackFxn),
    0,
};

#ifdef USE_MMU
CSL_ArmGicDistIntrf distrIntrf;
CSL_ArmGicCpuIntrf gCpuIntrf;

CSL_A15MmuLongDescObj mmuObj __attribute__((aligned(16 * 1024)));

CSL_A15MmuLongDescAttr mmuAttr0;
CSL_A15MmuLongDescAttr mmuAttr1;

static void mmu_init(void)
{
	uint32_t phyAddr = 0U;
	int32_t cacheType;

	cacheType = CSL_a15GetCacheType();

	if(cacheType & CSL_A15_CACHE_TYPE_ALL)
	{
		CSL_a15InvAllInstrCache();
		CSL_a15DisableCache();
	}

	mmuObj.numFirstLvlEntires = CSL_A15_MMU_LONG_DESC_LVL1_ENTIRES;
	mmuObj.numSecondLvlEntires = CSL_A15_MMU_LONG_DESC_LVL2_ENTIRES;
	mmuObj.mairEntires = CSL_A15_MMU_MAIR_LEN_BYTES;
	mmuObj.mairAttr[0] = 0x44U;
	mmuObj.mairAttr[1] = 0x00U;
	mmuObj.mairAttr[2] = 0xFFU;
	CSL_a15SetMmuMair(0, mmuObj.mairAttr[0]);
	CSL_a15SetMmuMair(1, mmuObj.mairAttr[1]);
	CSL_a15SetMmuMair(2, mmuObj.mairAttr[2]);

	CSL_a15InitMmuLongDesc(&mmuObj);

	CSL_a15InitMmuLongDescAttrs(&mmuAttr0);
	CSL_a15InitMmuLongDescAttrs(&mmuAttr1);

	mmuAttr0.type = CSL_A15_MMU_LONG_DESC_TYPE_BLOCK;
	mmuAttr0.accPerm = 0U;
	mmuAttr0.shareable = 2U;
	mmuAttr0.attrIndx = 2U;

	for (phyAddr = 0x00000000U; phyAddr < 0x08000000U; phyAddr += 0x00200000U)
		CSL_a15SetMmuSecondLevelLongDesc(&mmuObj, (void *)phyAddr,
						 (void *)phyAddr, &mmuAttr0);

	mmuAttr1.type = CSL_A15_MMU_LONG_DESC_TYPE_BLOCK;
	mmuAttr1.accPerm = 0U;
	mmuAttr1.shareable = 0U;
	mmuAttr1.attrIndx = 1U;

	for (phyAddr = 0x10000000U; phyAddr < 0x60000000U; phyAddr += 0x200000U)
		CSL_a15SetMmuSecondLevelLongDesc(&mmuObj, (void *)phyAddr,
						 (void *)phyAddr, &mmuAttr1);

	CSL_a15EnableMmu();
	CSL_a15EnableCache();
}
#endif

#ifdef USE_GIC
static void gic_init(void)
{
	gCpuIntrf.gicDist = &distrIntrf;
	gCpuIntrf.cpuIntfBasePtr = (void *)0x48212000U;
	distrIntrf.distBasePtr = (void *)0x48211000U;
	gCpuIntrf.initStatus = (Uint32)FALSE;
	gCpuIntrf.gicDist->initStatus = (Uint32)FALSE;
	gCpuIntrf.pDefaultIntrHandlers = NULL; /* &CSL_armGicDefaultHandler; */
	gCpuIntrf.pDefaultUserParameter = NULL;

	CSL_armGicInit(&gCpuIntrf);
}
#endif

/*******************************************************************************/
#define evmAM572x
#define SOC_AM572X

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ti/drv/gpio/GPIO.h>
#include <ti/drv/gpio/soc/GPIO_v1.h>

#include <ti/drv/uart/UART.h>
#include <ti/drv/uart/soc/UART_soc.h>
#include <ti/drv/uart/UART_stdio.h>
#include <ti/csl/soc.h>

#include <ti/board/board.h>
#include <ti/board/src/evmAM572x/include/board_cfg.h>

#define GPIO_PIN_VAL_LOW        (0U)
#define GPIO_PIN_VAL_HIGH       (1U)

#define NUMBER_OF_CYCLES        (3)

int led_test(void);
	
int main(void)
{
	Board_initCfg boardCfg;
	UART_HwAttrs uart_hwAttrs;
	Osal_HwAttrs osal_hwAttrs;
#ifdef USE_MMU
	mmu_init();
#endif	
#ifdef USE_GIC	
	gic_init();
#endif

	/* Make sure non-interrupt mode is used for UART */
	/* Get the UART default configuration */
	UART_socGetInitCfg(BOARD_UART_INSTANCE, &uart_hwAttrs);
	/* Disabling interrupt mode, forcing UART to use the polling mode */
	uart_hwAttrs.enableInterrupt=0; 
	/* Write back the config */
	UART_socSetInitCfg(BOARD_UART_INSTANCE, &uart_hwAttrs);
	
	Osal_getHwAttrs(&osal_hwAttrs);
	osal_hwAttrs.hwAccessType=OSAL_HWACCESS_RESTRICTED;
	Osal_setHwAttrs(OSAL_HWATTR_SET_HWACCESS_TYPE,&osal_hwAttrs);
	
	boardCfg = BOARD_INIT_MODULE_CLOCK | BOARD_INIT_UART_STDIO;

    /* Set the OSAL hwAccess to restricted */

	Board_init(boardCfg);

	return led_test();
}

void AppDelay(uint32_t delayVal)
{
	uint32_t cnt = 0;
	while(cnt < delayVal) {
		asm("");
		cnt++;
	}
}

int led_test()
{
	char p = 'r';
	int i, j, k;

	GPIO_init();

	UART_printf("\n*********************************************\n"); 
	UART_printf  ("*                 LED Test                  *\n");
	UART_printf  ("*********************************************\n");

	UART_printf("\nTesting LED\n");
	UART_printf("Blinking LEDs...\n");

	for (i=0; i<BOARD_GPIO_LED_NUM; i++)
		GPIO_write(i, GPIO_PIN_VAL_LOW);

	do {
		UART_printf("Press 'y' to verify pass, 'r' to blink again,\n");
		UART_printf("or any other character to indicate failure: ");
		for (i=0; i<NUMBER_OF_CYCLES; i++) {
			for (j=0; j<BOARD_GPIO_LED_NUM; j++) {
				for (k=0; k<BOARD_GPIO_LED_NUM; k++) {
					if (j==k)
						GPIO_write(k, GPIO_PIN_VAL_HIGH);
					else
						GPIO_write(k, GPIO_PIN_VAL_LOW);
				}
				AppDelay(5000000);
			}
		}

		UART_scanFmt("%c", &p);
		if (p == 'r')
			UART_printf("\nBlinking again\n");
	} while (p == 'r');

	for (i=0; i<BOARD_GPIO_LED_NUM; i++)
		GPIO_write(i, GPIO_PIN_VAL_LOW);

	UART_printf("Received: %c\n", p);
	if ( (p == 'y') || (p == 'Y') ) {
		UART_printf("\nTest PASSED!\n");
		return 0;
	}

	UART_printf("\nTest FAILED!\n");
	return -1;
}
